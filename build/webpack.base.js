const options = require('./options')
const path = require('path')

module.exports = {
  resolve: {
    modules: [
      path.join(__dirname, '../'),
      path.join(__dirname, '../node_modules')
    ],

    alias: {
      src: 'src',
      'vue$': 'vue/dist/vue.common.js'
    },

    extensions: ['.js', '.json', '.vue', '.scss']
  },

  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          loaders: {
            // configured in the script specific webpack configs
          },
        }
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/
      }
    ]
  },

  // Stats is used to customize Webpack's console output
  // https://webpack.js.org/configuration/stats/
  stats: {
    hash: false,
    colors: true,
    chunks: false,
    version: false,
    children: false,
    timings: true
  }
}
