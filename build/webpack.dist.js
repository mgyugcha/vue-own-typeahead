const merge = require('deep-assign')
const path = require('path')
const webpack = require('webpack')
const options = require('./options')
const base = require('./webpack.base')

console.log(options.isProduction)

const config = merge(base, {
  entry: './src/vue-own-typeahead.js',

  output: {
    filename: options.isProduction ?
      'vue-own-typeahead.min.js' : 'vue-own-typeahead.js',
    path: path.resolve(__dirname, '../dist'),
    library: 'vue-own-typeahead',
    libraryTarget: 'umd'
  },

  plugins: [ ],
})

config.plugins = config.plugins.concat([
  new webpack.LoaderOptionsPlugin({
    minimize: true
  })
])

if (options.isProduction) {
  config.plugins = config.plugins.concat([
    // Set the production environment
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"production"'
      }
    }),

    // Minify with dead-code elimination
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false
      }
    })
  ])
}

module.exports = config
