import Vue from 'vue'
import VueOwnTypeahead from '../src/vuejs-own-typeahead.vue'

var states = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California',
              'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii',
              'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana',
              'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota',
              'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire',
              'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota',
              'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island',
              'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont',
              'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'
             ]

var app = new Vue({
  el: '#app',
  components: {
    VueOwnTypeahead
  },
  data: {
    message: 'prueba',
    // states of usa
    typeaheadStates: {
      datasets: {
        name: 'states',
        source: new Bloodhound({
          datumTokenizer: Bloodhound.tokenizers.whitespace,
          queryTokenizer: Bloodhound.tokenizers.whitespace,
          local: states
        })
      }
    },
    // countries
    typeaheadCountries: {
      datasets: {
        name: 'countries',
        source: new Bloodhound({
          datumTokenizer: Bloodhound.tokenizers.whitespace,
          queryTokenizer: Bloodhound.tokenizers.whitespace,
          prefetch: 'https://twitter.github.io/typeahead.js/data/countries.json'
        })
      }
    },

    // Oscar winners for Best Picture
    typeaheadBestPictures: {
      datasets: {
        name: 'best-pictures',
        display: 'value',
        source: new Bloodhound({
          datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
          queryTokenizer: Bloodhound.tokenizers.whitespace,
          prefetch: {
            cache: false,
            url: 'https://twitter.github.io/typeahead.js/data/films/post_1960.json',
          },
          remote: {
            url: 'https://twitter.github.io/typeahead.js/data/films/queries/%QUERY.json',
            wildcard: '%QUERY'
          }
        }),
      }
    },

    // Oscar winners for Best Picture 2
    typeaheadBestPictures2: {
      datasets: {
        name: 'best-pictures',
        display: 'value',
        source: new Bloodhound({
          datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
          queryTokenizer: Bloodhound.tokenizers.whitespace,
          prefetch: {
            cache: false,
            url: 'https://twitter.github.io/typeahead.js/data/films/post_1960.json',
          },
          remote: {
            url: 'https://twitter.github.io/typeahead.js/data/films/queries/%QUERY.json',
            wildcard: '%QUERY'
          }
        }),
        templates: {
          empty: [
            '<div class="empty-message">',
            'Unable to find any Best Picture winners that match the current query',
            '</div>'
          ].join('\n'),
          suggestion: Handlebars.compile('<div><strong>{{value}}</strong> – {{year}}</div>')
        }
      }
    },
  },
  methods: {
    typeaheadSelect: function (e) {
      console.log('se seleccionó', e)
    }
  }
})
